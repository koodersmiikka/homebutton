-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 27.01.2019 klo 11:58
-- Palvelimen versio: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gartan_homes`
--

-- --------------------------------------------------------

--
-- Rakenne taululle `gamestate`
--

CREATE TABLE `gamestate` (
  `id` int(11) NOT NULL,
  `thing` varchar(50) NOT NULL,
  `value` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vedos taulusta `gamestate`
--

INSERT INTO `gamestate` (`id`, `thing`, `value`) VALUES
(1, 'gamegoing', '0'),
(2, 'currentround', '0');

-- --------------------------------------------------------

--
-- Rakenne taululle `players`
--

CREATE TABLE `players` (
  `id` int(11) NOT NULL,
  `name` varchar(15) NOT NULL,
  `jointime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vedos taulusta `players`
--

INSERT INTO `players` (`id`, `name`, `jointime`) VALUES
(102, 'Stuhl', '2019-01-27 07:06:34'),
(103, 'Fg', '2019-01-27 08:28:19');

-- --------------------------------------------------------

--
-- Rakenne taululle `results`
--

CREATE TABLE `results` (
  `id` int(11) NOT NULL,
  `round` int(11) NOT NULL,
  `nick` varchar(50) NOT NULL,
  `result` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Rakenne taululle `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `task` varchar(200) NOT NULL,
  `numbervalue` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  `doned` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vedos taulusta `tasks`
--

INSERT INTO `tasks` (`id`, `task`, `numbervalue`, `type`, `doned`) VALUES
(1, 'Keep orange button down for 3 seconds', 3000, 'hold', 0),
(2, 'Smash as many times as you can for 10 seconds!', 10, 'smash', 0),
(3, 'Hold button for 1 second', 1000, 'hold', 0),
(4, 'Smash for 1 second! Like a bunny!', 1, 'smash', 0),
(5, 'Hold the orange button for 10 seconds', 10000, 'hold', 0),
(6, 'Smash dat orange button for 5 seconds!', 5, 'smash', 0),
(7, 'Hold orange button for 20 seconds', 20000, 'hold', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gamestate`
--
ALTER TABLE `gamestate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `round` (`round`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gamestate`
--
ALTER TABLE `gamestate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
