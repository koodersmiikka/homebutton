const hcpath = 'http://localhost:8080/homebutton';

// Start game
$('#startButton').click(function(){
    if($('input[name=nick]').val() != ""){
        $.ajax({
        	type: "POST",
        	url: hcpath+'/tools/gameStart.php',
        	data: {'handle' : 'pick_nick' , 'nick' : $('input[name=nick]').val()},
        	success: function(data){
                $('#startButton').html('Joining...');
                localStorage.setItem("nick", $('input[name=nick]').val());
                window.setInterval(function(){
                    gameStatus();
                }, 3000);
        	},
        	error:function(exception){console.log(exception);}
        });
    }else{
        alert('Elite hacking detected - please give name!');
    }
});

$('#launchGame').click(function(){
    $.ajax({
        type: "POST",
        url: hcpath+'/tools/gameStart.php',
        data: {'handle' : 'game_start'},
        success: function(data){
            console.log(data);
            if(data == 1){
                console.log('ALKAA');
                window.location.href = hcpath + '/thegame.php?screen=desu';
            }else{
                $('#lobbydesu').html(data);
                console.log('Ei ala vielä');
            }
        },
        error:function(exception){console.log(exception);}
    });
});

$('#endGameButton').click(function(){
    $.ajax({
        type: "POST",
        url: hcpath+'/tools/gameStart.php',
        data: {'handle' : 'game_end'},
        success: function(data){
            window.location.href = hcpath + '?screen=desu';
        },
        error:function(exception){console.log(exception);}
    });
});

function getPlayerListLobby(){
    let nick = '';
    if(localStorage.getItem("nick") != '' && localStorage.getItem("nick") != 'undefined' && localStorage.getItem("nick") != null){ nick = localStorage.getItem("nick"); }
    console.log('nick on '+nick);
    $.ajax({
        type: "POST",
        url: hcpath+'/tools/gameStart.php',
        data: {'handle' : 'get_player_list' , 'nick' : nick},
        success: function(data){
            $('#lobbydesu').html(data);
        },
        error:function(exception){console.log(exception);}
    });
}

function getPlayerList(){
    $.ajax({
        type: "POST",
        url: hcpath+'/tools/gameStart.php',
        data: {'handle' : 'get_player_list'},
        success: function(data){
            $('#playerlist').html(data);
        },
        error:function(exception){console.log(exception);}
    });
}

function gameStatus(){
    let nick = '';
    if(localStorage.getItem("nick") != '' && localStorage.getItem("nick") != 'undefined' && localStorage.getItem("nick") != null){ nick = localStorage.getItem("nick"); }
    $.ajax({
        type: "POST",
        url: hcpath+'/tools/gameStart.php',
        data: {'handle' : 'game_status' , 'nick' : nick},
        success: function(data){
            console.log(data);
            if(data > 0){
                console.log('ALKAA');
                $('#lobbydesu').html('<h1>OMG THE GAME BEGINS!</h1>');
                setTimeout(function(){
                    window.location.href = hcpath + '/thegame.php?round='+data;
                },5000);
            }else{
                $('#lobbydesu').html(data);
                console.log('Ei ala vielä');
            }
        },
        error:function(exception){console.log(exception);}
    });
}

function gameStillRunning(){
    $.ajax({
        type: "POST",
        url: hcpath+'/tools/gameStart.php',
        data: {'handle' : 'game_running'},
        success: function(data){
            console.log(data);
            if(data == 0){
                console.log('END OF LINE');
                alert('GAME OVER BAIBAI!');
                localStorage.removeItem('nick')
                window.location.href = hcpath;
            }else{
                console.log('Game still running');
            }
        },
        error:function(exception){console.log(exception);}
    });
}

function getCurrentRound(){
    $.ajax({
        type: "POST",
        url: hcpath+'/tools/gameStart.php',
        data: {'handle' : 'current_round'},
        success: function(data){
            console.log(data);
            return data;
        },
        error:function(exception){console.log(exception);}
    });
}

function startCountdown(seconds){
    var timeleft = seconds;
    var downloadTimer = setInterval(function(){
        document.getElementById("countdown").innerHTML = timeleft + " seconds remaining";
        timeleft -= 1;
        if(timeleft <= 0){
            clearInterval(downloadTimer);
            document.getElementById("countdown").innerHTML = "Finished"
            saveSmashing();
        }
    }, 1000);

    setTimeout(function(){

    },seconds*100);
}

let timeheld = 0;
let amountOfTaps = 0;

$('#theButton').bind( "mousedown touchstart", function(e){
    if(roundtype == 'smash'){
        $(this).html(amountOfTaps);
    }else{
        $(this).html('o');
    }

    setLastClick(); // TIMED

    if(roundtype == 'smash' && amountOfTaps < 1){
        console.log('Alko timer');
        startCountdown(roundnumber);
    }
});

$('#theButton').bind( "mouseup touchend", function(e){
    if(roundtype == 'smash'){
        $(this).html(amountOfTaps);
    }else{
        $(this).html('O');
    }
    console.log(timeDifference()); // TIMED
    console.log(smash());

    if(roundtype == 'hold'){
        $.ajax({
            type: "POST",
            url: hcpath+'/tools/gameStart.php',
            data: {'handle' : 'save_result' , 'round' : currentRound , 'nick' : localStorage.getItem("nick") , 'result' : timeheld},
            success: function(data){
                data = JSON.parse(data);
                console.log(data);
                window.location.href = hcpath + '/greenroom.php?nextround='+data["nextround"];
            },
            error:function(exception){console.log(exception);}
        });
    }
});

function saveSmashing(){
    $.ajax({
        type: "POST",
        url: hcpath+'/tools/gameStart.php',
        data: {'handle' : 'save_result' , 'round' : currentRound , 'nick' : localStorage.getItem("nick") , 'result' : amountOfTaps},
        success: function(data){
            data = JSON.parse(data);
            console.log(data);
            window.location.href = hcpath + '/greenroom.php?nextround='+data["nextround"];
        },
        error:function(exception){console.log(exception);}
    });
}

function getFinalResults(){
    $.ajax({
        type: "POST",
        url: hcpath+'/tools/gameStart.php',
        data: {'handle' : 'get_final_results'},
        success: function(data){
            data = JSON.parse(data);
            console.log(data);
            $('#results').html(data['header']+data["winnings"]+'<hr style="margin-top:30px;">'+data['scores']);
        },
        error:function(exception){console.log(exception);}
    });
}

// TIMED BUTTON PRESS

let lastClick = 0;

function setLastClick(){
    let d = new Date();
    let t = d.getTime();

    lastClick = t;
}

function timeDifference(){
    let d = new Date();
    let t = d.getTime();

    timeheld = t-lastClick;
    return t-lastClick;
}

// Button SMASH

let howManyClicks = 0;

function smash(){
    howManyClicks++
    amountOfTaps = howManyClicks;
    return howManyClicks;
}