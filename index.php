<?php include_once 'blocks/header.php'; ?>

<a href="lobby.php<?php echo (isset($_GET["screen"]) ? '?screen=desu' : '') ?>" id="startButton" class="bigLink">GO!</a>

<script>
// If there's a game that started over 1 hour ago
$(document).ready(function(){
    $.ajax({
        type: "POST",
        url: hcpath+'/tools/gameStart.php',
        data: {'handle' : 'reset_game'},
        success: function(data){

        },
        error:function(exception){console.log(exception);}
    });
});
</script>

<?php include_once 'blocks/footer.php'; ?>