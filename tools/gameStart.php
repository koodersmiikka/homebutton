<?php include_once 'mysli.php';

if($_POST["handle"] == "pick_nick"){
    add_player($_POST["nick"],db());
}

if($_POST["handle"] == "game_status"){
    if(isGameRunning($_POST["nick"],db())){
        if(getCurrentRound(db()) == 0){
            echo playerList($_POST["nick"],db());
        }else{
            echo getCurrentRound(db());
        }
    }else{
        echo playerList($_POST["nick"],db());
    }
}

if($_POST["handle"] == "get_player_list"){
    echo playerList('admin',db());
}

if($_POST["handle"] == "game_start"){
    echo gameStart(db());
}

if($_POST["handle"] == "game_end"){
    echo gameEnd(db());
}

if($_POST["handle"] == "game_running"){
    echo gameRunningStatus(db());
}


if($_POST["handle"] == "run_game"){
    exit(json_encode(gimmeTask($_POST["round"],db())));
}

function gameStart($db){
    $sql = "UPDATE gamestate SET value = 1 WHERE thing = 'gamegoing' OR thing = 'currentround'";
    $q = $db->prepare($sql);
    $q->execute( array(

    ));

    return 1;
}

function gameEnd($db){
    $sql = "UPDATE gamestate SET value = 0 WHERE thing = 'gamegoing' OR thing = 'currentround'";
    $q = $db->prepare($sql);
    $q->execute( array(

    ));

    $sql = "DELETE FROM results";
    $q = $db->prepare($sql);
    $q->execute( array(

    ));

    return 1;
}

function add_player($nick,$db){
    $sql = "INSERT INTO players (name) VALUES (:name)";
    $q = $db->prepare($sql);
    $q->execute( array(
        ':name'=>$nick
    ));
}

function playerList($nick,$db){
    $getData = $db->prepare("SELECT * FROM players");
    $getData->execute();
    $data = $getData->fetchAll(PDO::FETCH_ASSOC);
    $playerlist = '<h1>WAIT FOR GAME TO START PLS</h1>';
    $playerlist .= '<h2>'.count($data).' players online!</h2>';
    for($i=0;$i<count($data);$i++){
        $playerlist .= ($nick == $data[$i]["name"] ? '<b>' : '').$data[$i]["name"].($nick == $data[$i]["name"] ? '</b>' : '').'<br>';
    }

    return $playerlist;
}

function isGameRunning($nick,$db){
    $getData = $db->prepare("SELECT * FROM gamestate WHERE thing = 'gamegoing'");
    $getData->execute();
    $data = $getData->fetch(PDO::FETCH_ASSOC);

    if($data["value"] == 0){
        return playerList($nick,db());
    }else{
        return 1;
    }
}

function gameRunningStatus($db){
    $getData = $db->prepare("SELECT * FROM gamestate WHERE thing = 'gamegoing'");
    $getData->execute();
    $data = $getData->fetch(PDO::FETCH_ASSOC);

    $sql = "DELETE FROM players";
    $q = $db->prepare($sql);
    $q->execute( array(

    ));

    return $data["value"];
}



/* GAMEPLAY */

function gimmeTask($round,$db){
    $getData = $db->prepare("SELECT * FROM tasks WHERE id = :roundnumber");
    $getData->bindParam(':roundnumber', $round, PDO::PARAM_INT);
    $getData->execute();
    $data = $getData->fetch(PDO::FETCH_ASSOC);

    return $data;
}

if($_POST["handle"] == "save_result"){
    exit(json_encode(saveResult($_POST["round"],$_POST["nick"],$_POST["result"],db())));
}

function saveResult($round,$nick,$result,$db){
    $getData = $db->prepare("SELECT * FROM results WHERE nick = :nick AND round = :roundnumber");
    $getData->bindParam(':roundnumber', $round, PDO::PARAM_INT);
    $getData->bindParam(':nick', $nick, PDO::PARAM_STR);
    $getData->execute();
    $datacheck = $getData->fetch(PDO::FETCH_ASSOC);

    if(!$datacheck){
        $sql = "INSERT INTO results (round, nick, result) VALUES (:round, :nick, :result)";
        $q = $db->prepare($sql);
        $q->execute( array(
            ':round'=>$round,
            ':nick'=>$nick,
            ':result'=>$result
        ));
    }

    $data["nextround"] = $round+1;

    return $data;
}

if($_POST["handle"] == "get_results"){
    exit(json_encode(getResults($_POST["round"],db())));
}

function getResults($round,$db){
    $returnResults["gameover"] = 0;

    $getData = $db->prepare("SELECT * FROM results WHERE round = :roundnumber");
    $getData->bindParam(':roundnumber', $round, PDO::PARAM_INT);
    $getData->execute();
    $data = $getData->fetchAll(PDO::FETCH_ASSOC);

    $taskgoal = gimmeTask($round,$db);
    $taskextension = giveExtension($taskgoal["type"]);

    $roundmax["winrar"] = 0;
    if($taskgoal["type"] == "smash"){
        $getData = $db->prepare("SELECT MAX(result) AS winrar FROM results WHERE round = :roundnumber");
        $getData->bindParam(':roundnumber', $round, PDO::PARAM_INT);
        $getData->execute();
        $roundmax = $getData->fetch(PDO::FETCH_ASSOC);
    }

    $holdtimes = [];
    if($taskgoal["type"] == "hold"){
        for($x=0;$x<count($data);$x++){
            $holdtimes[] = $data[$x]["result"];
        }

        $roundmax["winrar"] = getClosest($taskgoal["numbervalue"],$holdtimes);
    }

    $returnResults["scoreboard"] = '<h1>The goal for ROUND '.$taskgoal["id"].' was '.$taskgoal["numbervalue"].($taskgoal["type"] == "smash" ? 'sec MAX ' : '').$taskextension.'</h1>';

    for($i=0;$i<count($data);$i++){
        $returnResults["scoreboard"] .= ($roundmax["winrar"] == $data[$i]["result"] ? '<b>** ' : '').$data[$i]["nick"].': '.$data[$i]["result"].$taskextension.($roundmax["winrar"] == $data[$i]["result"] ? ' **</b>' : '').'<br />';
    }

    if(!gimmeTask($round,$db)){
        $returnResults["gameover"] = 1;
    }

    $returnResults["systemround"] = getCurrentRound($db);

    return $returnResults;
}

if($_POST["handle"] == "next_round"){
    exit(json_encode(nextRound($_POST["currentround"],db())));
}

function nextRound($currentround,$db){
    $sql = "UPDATE gamestate SET value = :nextround WHERE thing = 'currentround'";
    $q = $db->prepare($sql);
    $q->execute( array(
        ':nextround'=>$currentround+1
    ));

    $data["nextround"] = $currentround+1;

    return $data;
}

if($_POST["handle"] == "current_round"){
    echo getCurrentRound(db());
}

function getCurrentRound($db){
    $getData = $db->prepare("SELECT * FROM gamestate WHERE thing = 'currentround'");
    $getData->execute();
    $data = $getData->fetch(PDO::FETCH_ASSOC);

    return $data["value"];
}

if($_POST["handle"] == "get_final_results"){
    exit(json_encode(getFinalResults(db())));
}

function getFinalResults($db){
	$winnings = [];
    $data['header'] = '<h1>THE WINRARS ARE YOU!</h1>';

    $getData = $db->prepare("SELECT MAX(round) AS rounds FROM results");
    $getData->execute();
    $roundcheck = $getData->fetch(PDO::FETCH_ASSOC);
    $data['scores'] = '';
    for($i=0;$i<$roundcheck["rounds"];$i++){
        $round = $i+1;
        $getData = $db->prepare("SELECT * FROM results WHERE round = :roundnumber");
        $getData->bindParam(':roundnumber', $round, PDO::PARAM_INT);
        $getData->execute();
        $roundResults = $getData->fetchAll(PDO::FETCH_ASSOC);

        $taskgoal = gimmeTask($round,$db);
        $taskextension = giveExtension($taskgoal["type"]);

        $roundmax["winrar"] = 0;
        if($taskgoal["type"] == "smash"){
            $getData = $db->prepare("SELECT MAX(result) AS winrar FROM results WHERE round = :roundnumber");
            $getData->bindParam(':roundnumber', $round, PDO::PARAM_INT);
            $getData->execute();
            $roundmax = $getData->fetch(PDO::FETCH_ASSOC);
        }

        $holdtimes = [];
        if($taskgoal["type"] == "hold"){
            for($x=0;$x<count($roundResults);$x++){
                $holdtimes[] = $roundResults[$x]["result"];
            }

            $roundmax["winrar"] = getClosest($taskgoal["numbervalue"],$holdtimes);
        }

        $data["scores"] .= '<div class="oneThird"><h3>The goal for ROUND '.$taskgoal["id"].' was '.$taskgoal["numbervalue"].($taskgoal["type"] == "smash" ? 'sec MAX ' : '').$taskextension.'</h3>';
        for($x=0;$x<count($roundResults);$x++){
            $data["scores"] .= ($roundmax["winrar"] == $roundResults[$x]["result"] ? '<b>** ' : '').$roundResults[$x]["nick"].': '.$roundResults[$x]["result"].$taskextension.($roundmax["winrar"] == $roundResults[$x]["result"] ? ' **</b>' : '').'<br />';
            if($roundmax["winrar"] == $roundResults[$x]["result"]){
                $winnings[] = $roundResults[$x]["nick"];
            }
        }
        $data["scores"] .= '</div>';
    }
    $data["winnings"] = '';
    if(count($winnings) > 1){
        $tmp = array_count_values($winnings);
    	arsort($tmp);
		$d = 1;
    	foreach ($tmp as $key => $value){
    		$data["winnings"] .= $d.': '.$key.': '.$value.' rounds<br>';$d++;
    	}
    }

    return $data;
}

function getClosest($search, $arr) {
   $closest = null;
   foreach ($arr as $item) {
      if ($closest === null || abs($search - $closest) > abs($item - $search)) {
         $closest = $item;
      }
   }
   return $closest;
}

function giveExtension($type){
    switch ($type) {
        case 'hold':
            return 'ms';
            break;
        case 'smash':
            return ' smashes';
            break;
        default:

            break;
    }
}

if($_POST["handle"] == "reset_game"){
    if (strtotime(whenWasTheLastGame(db())) <= strtotime('-2 hours')) {
        gameEnd(db());
    }
}

function whenWasTheLastGame($db){
    $getData = $db->prepare("SELECT MIN(answertime) AS oldestAnswer FROM results");
    $getData->execute();
    $data = $getData->fetch(PDO::FETCH_ASSOC);

    return $data["oldestAnswer"];
}

?>