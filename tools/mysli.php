<?php
function db(){
    $dbname = "homebutton";
    $dbuser = "homebutton";
    $dbpw	= "homebutton";


    //Sivujen tietokanta yhdistys
    try {
        $dsn = 'mysql:host=localhost;dbname='.$dbname;
        $username = $dbuser;
        $password = $dbpw;
        $options = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        );

        $db = new PDO($dsn, $username, $password, $options);
        return $db;
    }catch(Exception $e){ echo 'Message: ' .$e->getMessage(); }
} ?>