<?php include_once 'blocks/header.php';

if(isset($_GET["screen"])){ ?>
    <div style="text-align: center;">
        <div id="results">
            <h1>Waiting for results for ROUND <span id="roundi"></span></h1>
        </div>

        <h3>Next round?</h3>
        <button id="nextRound" class="nicelink">NEXT ROUND!</button>
    </div>

<script>
$(document).ready(function(){
    $('#roundi').html(currentRound);
});

window.setInterval(function(){
    getResults();
}, 3000);

let currentRound = <?php echo (isset($_GET["round"]) ? $_GET["round"] : 1); ?>;

function getResults(){
    console.log('Haen tuloksia');
    $.ajax({
    	type: "POST",
        url: hcpath+'/tools/gameStart.php',
        data: {'handle' : 'get_results' , 'round' : currentRound},
    	success: function(data){
            data = JSON.parse(data);
    		$('#results').html(data["scoreboard"]);
            if(data["gameover"]){
                window.location.href = hcpath + '/scores.php?screen=desu';
            }
    	},
    	error:function(exception){console.log(exception);}
    });
}

$('#nextRound').click(function(){
    $.ajax({
    	type: "POST",
        url: hcpath+'/tools/gameStart.php',
        data: {'handle' : 'next_round' , 'currentround' : currentRound},
    	success: function(data){
            data = JSON.parse(data);
    		window.location.href = hcpath + '/thegame.php?round='+data["nextround"]+'&screen=desu';
    	},
    	error:function(exception){console.log(exception);}
    });
});
</script>
<?php }else{ ?>

<div id="task" class="splitbutton" style="height: 40vh;">
    Kenkae on ihana
</div>

<div id="countdown" class="splitbutton" style="height: 10vh;">

</div>

<button id="theButton" class="splitbutton">O</button>

<script>
$(document).ready(function(){
    runTheGame();
});

window.setInterval(function(){
    gameStillRunning();
}, 3000);

let currentRound = <?php echo (isset($_GET["round"]) ? $_GET["round"] : 1); ?>;
let roundtype = '';
let roundnumber = 0;

function runTheGame(){
    $.ajax({
    	type: "POST",
        url: hcpath+'/tools/gameStart.php',
        data: {'handle' : 'run_game' , 'round' : currentRound},
    	success: function(data){
            data = JSON.parse(data);
    		$('#task').html(data["task"]);
            roundtype = data["type"];
            roundnumber = data["numbervalue"];
            if(!data["task"]){
                window.location.href = hcpath + '/scores.php';
            }
    	},
    	error:function(exception){console.log(exception);}
    });
}
</script>

<?php } ?>

<?php include_once 'blocks/footer.php'; ?>