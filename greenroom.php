<?php include_once 'blocks/header.php'; ?>

<div id="results" style="text-align: center;">
    <h1>Waiting for results for ROUND <?php echo (isset($_GET["nextround"]) ? $_GET["nextround"]-1 : 1) ?></h1>
</div>

<h2 style="text-align: center;" id="secondary">Waiting for the Screen admin to do something</h2>

<script>
window.setInterval(function(){
    getResults();
}, 3000);

let currentRound = <?php echo (isset($_GET["nextround"]) ? $_GET["nextround"]-1 : 1) ?>;

function getResults(){
    console.log('Haen tuloksia');
    $.ajax({
    	type: "POST",
        url: hcpath+'/tools/gameStart.php',
        data: {'handle' : 'get_results' , 'round' : currentRound},
    	success: function(data){
            data = JSON.parse(data);
    		$('#results').html(data["scoreboard"]);
            if(data["systemround"] == <?php echo $_GET["nextround"] ?>){
                $('#results').html('<h1>ROUND '+data["systemround"]+' commencing</h1>');
                $('#secondary').html('');
                setTimeout(function(){
                    window.location.href = hcpath + '/thegame.php?round='+data["systemround"];
                },3000);
            }
    	},
    	error:function(exception){console.log(exception);}
    });
}
</script>

<?php include_once 'blocks/footer.php'; ?>